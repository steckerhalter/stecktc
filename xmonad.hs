import XMonad
import XMonad.Config.Xfce
import Data.Monoid (All (All), mappend)
import System.IO
import System.Exit

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.InsertPosition
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Layout.ResizableTile
import XMonad.Layout.NoBorders
import XMonad.Util.EZConfig
import XMonad.Util.Paste
import XMonad.Util.Run(spawnPipe)

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

myTerminal      = "x-terminal-emulator"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myBorderWidth   = 1
myModMask       = mod4Mask
myNumlockMask   = mod2Mask
myWorkspaces    = ["1","2","3","4","5","6"]
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

myKeys = \conf -> mkKeymap conf $
    [ ("M-<Space>",       spawn $ XMonad.terminal conf)
    , ("M-S-<Space>",     setLayout $ XMonad.layoutHook conf)
    , ("M-<Backspace>",   spawn "urxvt")
    , ("M-c",             kill)
    , ("M-0",             sendMessage NextLayout)
    , ("M-b",             refresh)
    , ("M-<Tab>",         windows W.focusDown)
    , ("M-j",             windows W.focusDown)
    , ("M-k",             windows W.focusUp)
    , ("M-m",             windows W.focusMaster)
    , ("M-<Return>",      windows W.swapMaster)
    , ("M-S-j",           windows W.swapDown)
    , ("M-S-k",           windows W.swapUp)
    , ("M-h",             sendMessage Shrink)
    , ("M-l",             sendMessage Expand)
    , ("M-S-h",           sendMessage MirrorShrink)
    , ("M-S-l",           sendMessage MirrorExpand)
    , ("M-t",             withFocused $ windows . W.sink)
    , ("M-,",             sendMessage (IncMasterN 1))
    , ("M-.",             sendMessage (IncMasterN (-1)))
    , ("M-S-y",           io (exitWith ExitSuccess))
    , ("M-y",             spawn "xmonad --recompile; xmonad --restart")

    , ("M-o",             moveTo Next HiddenWS)
    , ("M-i",             moveTo Prev HiddenWS)
    , ("M-S-o",           shiftTo Next HiddenWS >> moveTo Next HiddenWS)
    , ("M-S-i",           shiftTo Prev HiddenWS >> moveTo Prev HiddenWS)
    , ("M-M1-o",          shiftTo Next HiddenWS)
    , ("M-M1-i",          shiftTo Prev HiddenWS)
    , ("M-u",             nextScreen)
    , ("M-S-7",           swapNextScreen)
    , ("M-7",             swapNextScreen >> nextScreen)
    , ("M-S-8",           shiftNextScreen)
    , ("M-8",             shiftNextScreen >> nextScreen)
    , ("M-9",             withFocused toggleFloating)
    , ("M-;",             goToSelected defaultGSConfig)

    , ("M-p M-i",         spawn "x-www-browser")
    , ("M-p M-j",         spawn "emacsclient -c -a ''")
    , ("M-p M-n",         spawn "nautilus")
    , ("M-p M-p",         spawn "dmenu_run")
    , ("M-p M-o",         spawn "firefox")
    , ("M-p M-u",         spawn "urxvt")
    , ("M-p M-<Space>",   spawn "chromium https://www.google.com/search?q=\"$(xclip -o)\"")
    , ("M-p e",           spawn "xset r rate 200 70; setxkbmap us,ch -option grp:shifts_toggle,ctrl:swapcaps")
    , ("M-p n",           spawn "emacsclient -a \"\" -c -n ~/zebra/2013.tks")
    , ("M-p o",           spawn "emacsclient -a \"\" -c -n ~/org/work.org")
    , ("M-p s",           spawn "xclip -selection 'clipboard' .xmonad/paste.signature")
    , ("M-p w",           spawn "xset r rate 200 70; setxkbmap us,ch -option grp:shifts_toggle,ctrl:aa_ctrl,ctrl:swapcaps")
    , ("M-p x",           spawn "xdisplay --left-of normal")
    , ("M-p c",           spawn "xdisplay --right-of left")
    , ("M-p z",           spawn "xdisplay --above normal")
    , ("M-p m",           spawn "if `xinput list-props bcm5974 | grep 'Device Enabled' | egrep -q 1$`; then xinput disable bcm5974; else xinput enable bcm5974;  fi")

    , ("M-n M-j",         spawn "spc sudo")
    , ("M-n M-m",         spawn "spc")
    , ("M-n M-n",         spawn "spc copy")
    , ("M-n M-k",         spawn "spc www")

    ]
    ++
    [(m ++ k, windows $ f w)
        | (w, k) <- zip (XMonad.workspaces conf) (map show [1..9])
        , (m, f) <- [("M-",W.greedyView), ("M-S-",W.shift)]]
    -- ++
    -- [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
    --     | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
    --     , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

toggleFloating w = windows (\s -> if M.member w (W.floating s)
                                  then W.sink w s
                                  else (W.float w (W.RationalRect 0 0 1 0.95) s))

myLayout = avoidStruts $ smartBorders ( rt ||| Mirror rt)
  where
    rt =  ResizableTall 1 (5/100) (1/2) []

myManageHook =
    [ isFullscreen --> (doF W.focusDown <+> doFullFloat)
    , className =? "MPlayer"             --> doFloat
    , className =? "Zenity"              --> doFloat
    , className =? "Gtk-recordmydesktop" --> doFloat
    , className =? "Xfce4-notifyd"       --> doIgnore
    , resource  =? "desktop_window"      --> doIgnore
    , resource  =? "kdesktop"            --> doIgnore
    ]

main = xmonad $ xfceConfig
        { terminal           = myTerminal
        , modMask            = myModMask
        , workspaces         = myWorkspaces
        , manageHook         = composeOne [ isDialog -?> doFloat, fmap Just $ insertPosition Below Newer ] <+> manageHook xfceConfig <+> composeAll myManageHook
        , keys               = myKeys
        , layoutHook         = myLayout
        , handleEventHook    = fullscreenEventHook `mappend` handleEventHook xfceConfig
        }
