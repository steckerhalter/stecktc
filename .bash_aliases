# -*- shell-script -*-
shopt -s autocd
shopt -s extglob

# -- exports ------------------------------------------------------------------

export EDITOR=vi
export MU_COLORS=1
export CDPATH=.:~:~/xdg:~/etc:~/var:~/doc:~/share:/etc:/var

# -- colors --------------------------------------------------------------------

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# syntax highlight less
alias ca='pygmentize -g'
export LESS=-R

# -- directories/files ---------------------------------------------------------

alias ll='ls -l'
alias lh='ls -lh'

alias la='ls -A'
alias l='ls -CF'

alias b='cd ..'
c () { cd "$@";ls; }

alias 0='cd -'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

alias igrep='grep -i'

alias le='less'
#colored less (via pygmentize)
alias lc='LESSOPEN="|pygmentize -g %s" less'
alias lf='less +F'
alias lg='less +G'

alias tf='tail -50f'
alias t='tail'

# make a temp dir and change to it
alias td='cd $(mktemp -d)'
alias rmrf='rm -rf'
# output full path to file
alias re='readlink -e'
md () { mkdir -p "$@" && cd "$@"; }

# -- emacs ---------------------------------------------------------------------

# open terminal frame
alias et='emacsclient -a "" -t'
# open new gui frame
alias e='emacsclient -a "" -c -n'
# open existing gui frame
alias eo='emacsclient -a "" -n'
# emacs without init file and splash
alias eq='emacs -q --no-splash'
# open file with sudo
function se() {
    emacsclient -n -c -a "" "/sudo::$@"
}

# -- commands ------------------------------------------------------------------

alias cs='apt search'
alias o='xdg-open'
alias de='translate en:de'
alias en='translate de:en'
alias u='sudo su -'

# -- extensions ----------------------------------------------------------------

# liquid prompt https://github.com/nojhan/liquidprompt
if [[ -d "$HOME/etc/liquidprompt" ]]
then
    source ~/etc/liquidprompt/liquidprompt
fi

# git clone https://github.com/clvv/fasd
# cd fasd
# PREFIX=$HOME make install
if $(which fasd &> /dev/null)
   then
       eval "$(fasd --init auto)"
fi
