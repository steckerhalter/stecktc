# stecktc
steckerhalter's etc or "stecktc": various config and dotfiles

## dependencies

## setup
- bash
- python-pygments (for syntax highlighted `less`)

### core

I create a directory `etc` in my home. `CDPATH` which is set in `.bash_aliases`  makes it possible to easily cd to directories inside `~/etc`.

```shell
cd
mkdir etc
cd etc
git clone https://github.com/steckerhalter/stecktc
cd
# useful bash aliases and helpers
ln -s etc/stecktc/.bash_aliases
# git configuration and aliases (local config should be put in .gitconfig.local (like email etc.)
ln -s etc/stecktc/.gitconfig
# readline config
ln -s etc/stecktc/.inputrc
```

This assumes that `.bashrc` contains code to include `.bash_aliases` which is the case in Debian. Check your `.bashrc` if it contains something like this:

```shell
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

If it's missing then add it.

### [liquidprompt](https://github.com/nojhan/liquidprompt)

> A full-featured & carefully designed adaptive prompt for Bash & Zsh

```shell
cd ~/etc
git clone https://github.com/nojhan/liquidprompt
cd ~/.config/
ln -s ../etc/stecktc/liquidpromptrc
```

### [fasd](https://github.com/clvv/fasd)

```shell
cd ~/etc
git clone https://github.com/clvv/fasd
cd fasd
PREFIX=$HOME make install
```

Make sure that `.profile` adds `~/bin` to `PATH`. Add this to `.profile` if necessary:

```shell
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
```

It usually requires that you log out and back into your system.

## usage

| alias | description                        | command                           |
|-------|------------------------------------|-----------------------------------|
| cs    | search debian apt cache            | apt search                        |
| re    | output full path to file           | readlink -e                       |
| md    | make dir and change to it          | bash-function                     |
| et    | open emacs terminal frame          | emacsclient -a "" -t              |
| e     | open new emacs gui frame           | emacsclient -a "" -c -n           |
| eo    | open existing emacs gui frame      | emacsclient -a "" -n              |
| eq    | emacs without init file and splash | emacs -q --no-splash              |
| se    | open file in emacs with sudo       | bash-function                     |
| u     |                                    | sudo su -                         |
| o     |                                    | xdg-open                          |
| b     |                                    | cd ..                             |
| 0     |                                    | cd -                              |
| ..    |                                    | cd ..                             |
| le    |                                    | less                              |
| cl    | colored less (pygmentize)          | LESSOPEN="|pygmentize -g %s" less |
| t     |                                    | tail                              |
| tf    |                                    | tail -50f                         |
| td    |                                    | make a temp dir and change to it  |

**document readline shortcuts!**
